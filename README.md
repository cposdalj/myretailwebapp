# myRetailWebApp

This application is a proof of concept Web API to perform GET and PUT requests for product information.

## Technology/ Frameworks
Java 8

Spring Boot 2.1.5

Maven 4.0.0

MongoDB 4.1.0


## Installation

Download the project using "git clone" then "cd" to app folder. See commands below. 

```bash
git clone https://cposdalj@bitbucket.org/cposdalj/myretailwebapp.git

cd myRetailWebApp
```
Assuming your machine has mongodb run and start the mongo shell. Create the database and products collection. Then use "insertMany" to insert sample data. See commands below. 

```bash
mongo

use myRetailDB

db.createCollection('products');

db.products.insertMany([[
	{_id: 13860416, value: 10.02, currency_code: "USD"},
	{_id: 13860418, value: 10.30, currency_code: "USD"},
	{_id: 13860419, value: 12.02, currency_code: "CNY"},
	{_id: 13860420, value: 12.42, currency_code: "CNY"},
	{_id: 13860424, value: 9.02, currency_code: "INR"},
	{_id: 13860425, value: 10.58, currency_code: "INR"},
	{_id: 13860427, value: 15.02, currency_code: "MXN"},
	{_id: 13860428, value: 11.02, currency_code: "MXN"},
	{_id: 13860429, value: 10.78, currency_code: "EUR"},
	]); 

```

Assuming your machine has maven and java installed (see versions above), run "mvn install" to build and package java jar file. Navigate to myRetailWebApp/target folder then run the jar file (see commands below). 

```bash
mvn install 

java -jar myRetailWebApp-0.0.1-SNAPSHOT.jar
```

Another option is using a .war file to deploy directly to a tomcat instance, but I have not configured the pom.xml to package as war. 


## Usage

### GET Request
Performs HTTP GET on product/{id} where {id} = product id. Retrieves and aggregates json data from local mongodb and consumes API from redsky.target. 

Input: http://localhost:8181/products/13860418

Output: 

```json
{"_id":13860418,"name":"Some Girls:Live In Texas 78 (DVD)","value":"10.3","currency":"USD"}
```
### PUT Request
Performs HTTP PUT on product/{id} with a Product Object as RequestBody. Product in mongo db with selected id has it's value updated and saved.
You must use Postman or other API Testing tool to confirm this request. 

In Postman select PUT, enter url: http://localhost:8181/products/13860416, under body select raw and JSON(application/json). In the text editor copy and paste sample below. The id in the url must match the id in the json request body.

```json
{
	"_id": 13860416, 
	"value": "10.13", 
	"currency_code": "USD"
}
```
After pressing Send a succesful PUT request will return a "Status: 200 OK"

## Road to Production 
1. Implement logic on PUT request to identify mismatch between path variable id and requestBody id.
2. Add unit testing for PUT request, where mongo save operation unsuccessful. 
3. Research and create more robust test cases.
4. Enhance custom exceptions.
5. Utilize log4j for detailed logging.
6. Deploy application to AWS/Azure instance.
7. Utilize CI/CD tools such as docker (container management) and Jenkins (build/test/deploy automation)
8. Secure the application: get ssl certificate and enable https within app.