package com.webservices.myRetailWebApp;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import com.webservices.myRetailWebApp.Exception.ProductNotFoundExternalAPIException;
import com.webservices.myRetailWebApp.Exception.ProductNotFoundMongoException;
import com.webservices.myRetailWebApp.Exception.ProductNotUpdatedException;
import com.webservices.myRetailWebApp.Repository.ProductRepository;
import com.webservices.myRetailWebApp.Service.PriceUpdateImpl;
import com.webservices.myRetailWebApp.Service.ProductNameExternalAPIImpl;
import com.webservices.myRetailWebApp.Service.ProductServiceImpl;
import com.webservices.myRetailWebApp.model.Product;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MyRetailWebAppApplicationTests {
	
	@Autowired 
	private ProductServiceImpl prodService;
	
	@Autowired 
	private ProductRepository prodRepo;
	
	@Autowired
	private ProductNameExternalAPIImpl prodPriceExtAPI;
	
	@Autowired
	private PriceUpdateImpl priceUpdate;
	

	//Test for applying productservice on known ID in both mongodb and redsky
	@Test public void prodServiceTest() throws ProductNotFoundMongoException, ProductNotFoundExternalAPIException { 
		//id for product exists in both local mongodb and redsky.target.com
		Product product420 = prodService.findById(13860420);
		//Test id 
		assertEquals(product420.get_id(), 13860420);
		//Test value
		assertEquals(product420.getValue(), "12.42");
		//Test currency_code
		assertEquals(product420.getName(), "Final Destination 5 (dvd_video)");
		//Test currency_code
		assertEquals(product420.getCurrency(), "CNY");
	  }
	
	/*
	 * Test for applying productservice on known ID that exists in redsky but not in
	 * mongodb. Expecting ProductNotFoundMongoException.
	 */	 
	 @Test(expected = ProductNotFoundMongoException.class)
	  public void prodNotFoundMongoTest() throws ProductNotFoundMongoException, ProductNotFoundExternalAPIException{ 
		  //This id exists in redsky but is not present in local mongodb 
		  prodService.findById(13860432);
	  }
	
	
	//Test to retrieve json data from mongodb via mongorepository
	@Test
	public void prodRepoTest() {
		//id from local mongodb collection "db.products"
		Product product416 = prodRepo.findbyId(13860416);
		//Test id 
		assertEquals(product416.get_id(), 13860416);
		//Test value
		assertEquals(product416.getValue(), "10.13");
		//Test currency_code
		assertEquals(product416.getCurrency(), "USD");
	}
	
	//Test to retrieve confirmed title json from redsky API
	  @Test 
	  public void prodPriceExtAPITest() throws ProductNotFoundExternalAPIException { 
		  //id from redsky.target.com 
		  assertEquals(prodPriceExtAPI.getProductNameFromID(13860428), "The Big Lebowski (Blu-ray)");
	  }
	
	  //Test to retrieve confirmed does not exist title json from redsky API
	  @Test(expected = ProductNotFoundExternalAPIException.class)
	  public void prodPriceExtAPIExceptionTest() throws ProductNotFoundExternalAPIException { 
		  //id from redsky.target.com 
		  prodPriceExtAPI.getProductNameFromID(010101010);
	  }
	  //Test to get succesful update of price
	  @Test
	  public void priceUpdateSuccesTest() throws ProductNotFoundMongoException, ProductNotUpdatedException{
		  //use id of known product to get Product object
		  Product prod1 = prodRepo.findbyId(13860418);
		  String valueOriginal = prod1.getValue();
		  //change value
		  double testDouble = Double.parseDouble(prod1.getValue());
		  testDouble += 10;
		  String newValue = String.valueOf(testDouble);
		  prod1.setValue(newValue);
		  //call method with new Value
		  priceUpdate.updatePriceById(prod1);
		  //perform findById operation
		  Product prod2 = prodRepo.findbyId(13860418);
		  //Make sure values are different to confirm value was updates
		  assertFalse(valueOriginal.equals(prod2.getValue()));
		  
	  }
	  
	  //Test to invoke ProductNotFoundMongoException from updatePrice (id does not exist)
	  @Test(expected = ProductNotFoundMongoException.class)
	  public void priceUpdateMongoExceptionTest() throws ProductNotFoundMongoException, ProductNotUpdatedException { 
		  Product test = new Product(111111,"", "12.34", "CNY");
		  priceUpdate.updatePriceById(test);
	  }
	 //Test needed to invoke ProductNotFoundMongoException from updatePrice

}
