package com.webservices.myRetailWebApp.Repository;

/*
 * Simple abstracted class to provide a model for CRUD operations to DB
 * Spring Data MongoDB creates implementation no need to create ProductRepositoryImpl Class
 */
import com.webservices.myRetailWebApp.model.Product;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRepository extends MongoRepository<Product, Integer>{
	
	//Custom JSON Query for getting product by _id
	@Query("{'_id' : ?0}")
	Product findbyId(int _id);
}
