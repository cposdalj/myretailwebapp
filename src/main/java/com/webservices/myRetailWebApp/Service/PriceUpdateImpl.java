package com.webservices.myRetailWebApp.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.webservices.myRetailWebApp.Exception.ProductNotFoundMongoException;
import com.webservices.myRetailWebApp.Exception.ProductNotUpdatedException;
import com.webservices.myRetailWebApp.Repository.ProductRepository;
import com.webservices.myRetailWebApp.model.Product;

@Service
public class PriceUpdateImpl implements PriceUpdate{

	@Autowired
	ProductRepository pr1;
	
	@Override
	public void updatePriceById(Product product) throws ProductNotFoundMongoException, ProductNotUpdatedException{
		Product productTemp = pr1.findbyId(product.get_id());
		if(productTemp == null) {
			throw new ProductNotFoundMongoException(product.get_id());
		}
		//manually set the value 
		productTemp.setValue(product.getValue());
		//save = mongorepositories update action
		Product savedProduct = pr1.save(productTemp);
		if(savedProduct == null) {
			throw new ProductNotUpdatedException(product.get_id());
		}
		
	}}
