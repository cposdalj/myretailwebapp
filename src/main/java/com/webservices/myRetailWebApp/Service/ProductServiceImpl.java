package com.webservices.myRetailWebApp.Service;
/*
 * Service class to retrieve product information by id from local db
 * ProductRepository is used to perform CRUD operation on local db
 * ProductPriceExternalAPIImpl is used to consume external API and retrieve title from redsky 
 */
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.webservices.myRetailWebApp.Exception.ProductNotFoundExternalAPIException;
import com.webservices.myRetailWebApp.Exception.ProductNotFoundMongoException;
import com.webservices.myRetailWebApp.Repository.ProductRepository;
import com.webservices.myRetailWebApp.model.Product;

@Service
public class ProductServiceImpl implements ProductService{

	@Autowired
	private ProductRepository pr;
	
	@Autowired
	private ProductNameExternalAPIImpl ppextAPI;
	
	@Override
	public Product findById(int id) throws ProductNotFoundMongoException, ProductNotFoundExternalAPIException{
		String title = ppextAPI.getProductNameFromID(id);
		Product retrievedProduct;
		
		//Product model is returned from productRepository findById
		Product productModel = pr.findbyId(id);
		if(productModel == null) {
			throw new ProductNotFoundMongoException(id);
		}
		//Product title is added to this new Product object before returning
		retrievedProduct = new Product(productModel.get_id(), title, productModel.getValue(), productModel.getCurrency());
		
		return retrievedProduct;
	}
}
