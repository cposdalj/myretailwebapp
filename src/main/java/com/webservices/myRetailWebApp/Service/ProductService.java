package com.webservices.myRetailWebApp.Service;
import com.webservices.myRetailWebApp.Exception.ProductNotFoundExternalAPIException;
import com.webservices.myRetailWebApp.Exception.ProductNotFoundMongoException;
/*
 * Interface for retrieving product information by Id from db
 */
import com.webservices.myRetailWebApp.model.*;

public interface ProductService {
	
	Product findById(int _id) throws ProductNotFoundMongoException, ProductNotFoundExternalAPIException;
}
