package com.webservices.myRetailWebApp.Service;

import com.webservices.myRetailWebApp.Exception.ProductNotFoundMongoException;
import com.webservices.myRetailWebApp.Exception.ProductNotUpdatedException;
import com.webservices.myRetailWebApp.model.Product;

public interface PriceUpdate {
	void updatePriceById(Product product) throws ProductNotFoundMongoException, ProductNotUpdatedException;
}
