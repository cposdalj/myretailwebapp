package com.webservices.myRetailWebApp.Service;

import com.webservices.myRetailWebApp.Exception.ProductNotFoundExternalAPIException;

/*
 * Interface for getting product name from redsky 
 */
public interface ProductNameExternalAPI {
	
	String getProductNameFromID(int id) throws ProductNotFoundExternalAPIException;

}
