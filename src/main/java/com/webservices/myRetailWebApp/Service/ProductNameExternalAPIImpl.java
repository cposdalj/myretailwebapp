package com.webservices.myRetailWebApp.Service;
/*
 * Service class to retrieve json from redsky using product id
 * RestTemplate used to consume redsky API
 * JsonPath used to specify path to nested json
 */
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import com.jayway.jsonpath.JsonPath;
import com.webservices.myRetailWebApp.Exception.ProductNotFoundExternalAPIException;

@Service
public class ProductNameExternalAPIImpl implements ProductNameExternalAPI{
	
	private static final String REDSKYURL = "https://redsky.target.com/v2/pdp/tcin/";
	private static final String REDSKYPARAM = "?excludes=taxonomy,price,promotion,bulk_ship,rating_and_review_reviews,rating_and_review_statistics,question_answer_statistics";

	@Override
	public String getProductNameFromID(int id) throws ProductNotFoundExternalAPIException{
		String title;
		
		RestTemplate restTemp = new RestTemplate();
		//retrieves JSON as a string from request
		try {
			String rawJson = restTemp.getForObject(REDSKYURL + id + REDSKYPARAM, String.class);
			//retrieves json object specified for nested item "title"
			title = JsonPath.read(rawJson, "$.product.item.product_description.title");
			
		}
		catch (HttpClientErrorException ex){
			throw new ProductNotFoundExternalAPIException(id);
		}
		
		
		return title;
		
	}

}
