package com.webservices.myRetailWebApp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MyRetailWebAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(MyRetailWebAppApplication.class, args);
	}

}
