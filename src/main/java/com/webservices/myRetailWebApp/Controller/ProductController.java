package com.webservices.myRetailWebApp.Controller;
/*
 * Controller class for REST API
 * All API calls are in /product
 * Mapping for /{id} ta
 */
import com.webservices.myRetailWebApp.model.Product;
import com.webservices.myRetailWebApp.Exception.ProductNotFoundExternalAPIException;
import com.webservices.myRetailWebApp.Exception.ProductNotFoundMongoException;
import com.webservices.myRetailWebApp.Exception.ProductNotUpdatedException;
import com.webservices.myRetailWebApp.Service.PriceUpdateImpl;
import com.webservices.myRetailWebApp.Service.ProductServiceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/products")
public class ProductController {
	
	
	@Autowired private ProductServiceImpl prodService ;
	
	@Autowired private PriceUpdateImpl updateService;
	
	/*
	 * Uses spring boots shortcut annotation for GET request
	 * Takes product Id from url and uses ProductServiceImpl class to 
	 * retrieve product information from mongodb and redsky API
	 */
	@GetMapping("/{id}") public Product getProductById(@PathVariable("id") int id){ 	
		try {
			Product response =  prodService.findById(id);
			
			return response;
		}
		catch (ProductNotFoundMongoException ex) {
			throw new ResponseStatusException (HttpStatus.NOT_FOUND, ex.getMessage());
		}
		catch(ProductNotFoundExternalAPIException ex) {
			throw new ResponseStatusException (HttpStatus.NOT_FOUND, ex.getMessage());
		}
	}
	
	/*
	 * Uses spring boots shortcut annotation for PUT request
	 * takes id as path variable and Product object as Json Body
	 * Updates the value in mongodb using provided value in json Body
	 */
	@PutMapping("/{id}")
	public void updatePrice(@PathVariable("id") int id, @RequestBody Product productModel) {
		try {
			updateService.updatePriceById(productModel);
		}
		catch(ProductNotFoundMongoException ex) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, ex.getMessage());
		}
		catch(ProductNotUpdatedException ex) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage());
		}
	}

	/*
	 * Simple request to display app health
	 */
	@RequestMapping("/IsAlive")
	public String IsAlive() {
		return "products IsAlive call succesful";
	}

}
