package com.webservices.myRetailWebApp.Exception;

public class ProductNotFoundMongoException extends Exception{
	
	public ProductNotFoundMongoException(int id) {
		super("Product with id: " + id + " not found in mongo database");
	}
}
