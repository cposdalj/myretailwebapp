package com.webservices.myRetailWebApp.Exception;

public class ProductNotFoundExternalAPIException extends Exception{
	
	public ProductNotFoundExternalAPIException(int id) {
		super("Product with id: " + id + " was not able to be retrieved from endpoint https://redsky.target.com/v2/pdp/tcin/" + id + 
			  "?excludes=taxonomy,price,promotion,bulk_ship,rating_and_review_reviews,rating_and_review_statistics,question_answer_statistics");
	}
}
