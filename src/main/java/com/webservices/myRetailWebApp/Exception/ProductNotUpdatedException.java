package com.webservices.myRetailWebApp.Exception;

public class ProductNotUpdatedException extends Exception{
	
	public ProductNotUpdatedException(int id) {
		super("Product with id: " + id + " was not succesfully updated in mongo DB");
	}
}
