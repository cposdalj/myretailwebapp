package com.webservices.myRetailWebApp.model;
/*
 * Model Representation for Product from myRetailDBA products collection
 * _id is primary key in database
 * name is for product title
 * value is for price
 * currency_code is for country code
 */
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection="products")
public class Product {
	@Id
	private int _id;
	private String name;
	private String value;
	private String currency_code;
	
	//Constructor
	public Product() {};
	public Product(int _id, String name, String value, String currency_code) {
		this._id = _id;
		this.name = name;
		this.value = value;
		this.currency_code = currency_code;
	}
	
	//Getters & Setters
	public int get_id() {
		return _id;
	}
	public void set_id(int _id) {
		this._id = _id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public void setCurrency(String currency_code) {
		this.currency_code = currency_code;
	}
	public String getCurrency() {
		return currency_code;
	}
	
}
